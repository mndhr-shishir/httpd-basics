FROM httpd:alpine

# Enable RewriteModule
RUN sed -i 's/^#\(LoadModule .*mod_rewrite.so\)/\1/' /usr/local/apache2/conf/httpd.conf

# Enable htaccess
COPY conf/httpd.conf /tmp/partial.conf
RUN cat /tmp/partial.conf >> /usr/local/apache2/conf/httpd.conf

# Restrict access
RUN htpasswd -bc /usr/local/apache2/conf/passwds hello world
RUN chown daemon:daemon /usr/local/apache2/conf/passwds
RUN chmod 600 /usr/local/apache2/conf/passwds
